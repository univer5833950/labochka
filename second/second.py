import os
import psycopg2

def get_stud(pos_url: str, query: str):
    try:
        connection = psycopg2.connect(pos_url)
        cursor = connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        connection.commit()
        return result
    except Exception as e:
        print(e)

    finally:
        if cursor:
            cursor.close()
        if connection:
            connection.close()

if __name__ == '__main__':
    database_url = os.environ.get("POSTGRES_URL")
    query = """WITH avg_marks AS (
    SELECT stud_id, AVG(mark_subject) AS avg_mark
    FROM mark_card
    GROUP BY stud_id
    ORDER BY avg_mark ASC LIMIT 1)
    SELECT CONCAT(students.first_name, ' ', students.last_name) AS student_name,
        mark_card.subject_card AS subject, mark_card.mark_subject AS mark
    FROM students
    JOIN mark_card ON students.student_id = mark_card.stud_id
    JOIN avg_marks ON students.student_id = avg_marks.stud_id;"""

    res = get_stud(pos_url=database_url, query=query)
    print(res)
