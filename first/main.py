import os
from mimesis import Person

def generate_random_email():
    person = Person()
    email = person.email()

    # Разбиваем email на три строки
    name, rest = email.split('@', 1)
    domain, local = rest.split('.', 1)

    # Заменяем значения на рандомные, если они не заданы в переменных окружения
    name = os.environ.get('NAME', name)
    domain = os.environ.get('DOMAIN', domain)
    local = os.environ.get('LOCAL', local)

    email = f"{name}@{domain}.{local}"

    return email

if __name__ == "__main__":
    random_email = generate_random_email()
    print("Случайный email адрес:", random_email)